# mws-reports

This is Amazon's reports **Reports API Section Client Library**.
The same library is also used for the **Feeds** section of their API.

https://developer.amazonservices.co.uk/doc/bde/reports/v20090101/php.html